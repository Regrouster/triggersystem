﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TSTrigger : MonoBehaviour {
    public GameObject triggerSystem;
    public string[] tags;

    private TriggerSystem ts;
    private bool canTrigger = false;

    private void Start () {
        if ( tags.Length == 0 ) {
            Debug.Log ( "No tags" );
            return;
        }

        ts = triggerSystem.GetComponent<TriggerSystem> ();

        if ( ts == null ) {
            Debug.Log ( "There is no TriggerSystem" );
            return;
        }

        canTrigger = true;
    }

    private void OnTriggerEnter ( Collider other ) {
        if ( !canTrigger ) return;

        string tag = other.tag;
        bool isTag = false;
        for ( int i = 0; i < tags.Length; i++ ) {
            if ( tags[ i ] == tag ) {
                isTag = true;
                break;
            }
        }

        if ( !isTag ) return;

        ts.Parse ();
    }
}
