﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerButton : MonoBehaviour {
    public Transform raycastFrom;
    public float maxDistance = 1.5f;
    public string[] tags;
    public string key = "f";

    void Update () {
        if ( !Input.GetKeyDown ( key ) ) return;

        RaycastHit meow;
        Ray ray = new Ray (
            raycastFrom.position,
            raycastFrom.forward
        );

        if ( !Physics.Raycast ( raycastFrom.position, raycastFrom.forward, out meow, maxDistance ) ) {
            return;
        }

        BTNTrigger btnt = meow.collider.gameObject.GetComponent<BTNTrigger> ();

        if ( btnt == null ) {
            return;
        }

        btnt.Use ();
    }
}
