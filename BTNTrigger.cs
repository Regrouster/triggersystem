﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BTNTrigger : MonoBehaviour {
    public GameObject triggerSystem;

    private TriggerSystem ts;
    private bool canTrigger = false;

    public void Use () {
        Debug.Log ( "Use" );
        if ( !canTrigger ) return;

        ts.Parse ();
    }

    void Start () {
        ts = triggerSystem.GetComponent<TriggerSystem> ();

        if ( ts == null ) {
            Debug.Log ( "There is no TriggerSystem" );
            return;
        }

        canTrigger = true;
    }
}
