﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObjectBySteps : MonoBehaviour {
    public Transform target;
    public int steps = 20;

    private int currentStep = 0;
    private Vector3 startPosition;
    private Vector3 deltaPosition;
    private bool isMoving = false;

    public void MoveYourself () {
        currentStep = steps;

        startPosition = transform.position;
        deltaPosition = target.position - startPosition;

        isMoving = true;
    }

    private void Update () {
        if ( !isMoving ) return;

        float coef = ( float ) ( steps - currentStep ) / steps;
        
        transform.position = new Vector3 (
            startPosition.x + deltaPosition.x * coef,
            startPosition.y + deltaPosition.y * coef,
            startPosition.z + deltaPosition.z * coef
        );

        currentStep--;

        if ( currentStep == 0 ) {
            isMoving = false;
            transform.position = target.position;
        }
    }
}
