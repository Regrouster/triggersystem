﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObjectByTime : MonoBehaviour {
    public Transform target;
    public int time;

    private Vector3 startPosition;
    private Vector3 deltaPosition;
    private float startTime;
    private float deltaTime;
    private float endTime;

    private bool isMoving = false;

    public void MoveYourself () {
        startPosition = transform.position;
        deltaPosition = target.position - startPosition;

        startTime = Time.time;
        deltaTime = time * 0.001f;
        endTime = startTime + deltaTime;

        isMoving = true;
    }

    void Start () {

    }

    void Update () {
        if ( !isMoving ) return;

        if ( Time.time > endTime ) {
            isMoving = false;
            transform.position = target.position;
            return;
        }

        float coef = ( Time.time - startTime ) / deltaTime;
        
        transform.position = new Vector3 (
            startPosition.x + deltaPosition.x * coef,
            startPosition.y + deltaPosition.y * coef,
            startPosition.z + deltaPosition.z * coef
        );
    }
}
