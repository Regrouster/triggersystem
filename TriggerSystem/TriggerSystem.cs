﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerSystem : MonoBehaviour {
    public string[] code;

    public string[] strings;
    public GameObject[] objs;

    public bool   once             = true;
    public uint   delayBeforeReset = 1000;
    public uint   currentLine      = 0;

    public float deltaTime        = 0.0f;
    public bool  isTriggered      = false;
    public bool  isStay           = false;

    public float delayTime        = 0.0f;
    public bool  isTrigDelay      = false;
    private bool isParsed         = false;

    private char[] SPACE = { ' ', ',' };
    private char[] QUOTES = { ';' };

    private int ParseSomeInt ( string value ) {
        int val = 0;
        int k   = value.Length;

        if ( k == 0 ) return -1;

        for ( int i = 0; i < k; i++ ) {
            if ( value[ i ] >= '0' && value[ i ] <= '9' ) {
                val *= 10;
                val += value[ i ] - '0';
            } else {
                return -1;
            }
        }

        return val;
    }

    private void TriggerMove ( string a0, string a1 ) {
        int indexOrigin = 0;
        int indexTarget = 0;

        indexOrigin = ParseSomeInt ( a0 );
        indexTarget = ParseSomeInt ( a1 );

        if ( indexOrigin == -1 || indexTarget == -1 ) {
            Debug.Log ( "Can't parse " + a0 + " or " + a1 );
            return;
        }

        if ( objs.Length <= Mathf.Max ( indexOrigin, indexTarget ) ) {
            Debug.Log ( "Index out of range" );
            return;
        }

        if ( !objs[ indexOrigin ] || !objs[ indexTarget ] ) {
            Debug.Log ( "objs[" + indexOrigin + "] or objs[" + indexTarget + "] doesn't exist" );
            return;
        }

        objs[ indexOrigin ].transform.position = objs[ indexTarget ].transform.position;
    }

    private void TriggerLookAt ( string a0, string a1 ) {
        int indexOrigin = ParseSomeInt ( a0 );
        int indexTarget = ParseSomeInt ( a1 );

        if ( indexOrigin == -1 || indexTarget == -1 ) {
            Debug.Log ( "Can't parse " + a0 + " or " + a1 );
            return;
        }

        if ( objs.Length <= Mathf.Max ( indexOrigin, indexTarget ) ) {
            Debug.Log ( "Index out of range" );
            return;
        }

        if ( !objs[ indexOrigin ] || !objs[ indexTarget ] ) {
            Debug.Log ( "objs[" + indexOrigin + "] or objs[" + indexTarget + "] doesn't exist" );
            return;
        }

        objs[ indexOrigin ].transform.LookAt ( objs[ indexTarget ].transform.position );
    }

    private void TriggerLaunch ( string a0, string a1 ) {
        int indexObject    = ParseSomeInt ( a0 );

        if ( indexObject == -1 ) {
            Debug.Log ( "Can't parse " + a0 );
            return;
        }

        if ( objs.Length <= indexObject ) {
            Debug.Log ( "Object is out of range" );
            return;
        }

        if ( !objs[ indexObject ] ) {
            Debug.Log ( "Object is null" );
            return;
        }

        string str;
        int indexString;

        if ( a1[ 0 ] == '\"' ) {
            str = a1.Substring ( 1, a1.Length - 2 );
        } else {
            indexString = ParseSomeInt ( a1 );

            if ( indexString == -1 ) {
                Debug.Log ( "Can't parse string index " + a1 );
                return;
            }

            if ( strings.Length <= indexString ) {
                Debug.Log ( "String is out of range" );
                return;
            }

            str = strings[ indexString ];
        }

        objs[ indexObject ].SendMessage ( str );
    }

    private void TriggerSetActive ( string a0, string a1 ) {
        int indexObject = ParseSomeInt ( a0 );

        if ( indexObject == -1 ) {
            Debug.Log ( "Can't parse " + a0 );
            return;
        }

        if ( objs.Length <= indexObject ) {
            Debug.Log ( "Object is out of range" );
            return;
        }

        if ( !objs[ indexObject ] ) {
            Debug.Log ( "Object is null" );
            return;
        }

        bool state;

        if ( a1[ 0 ] == 'n' ) {
            state = !objs[ indexObject ].activeSelf;
        } else {
            state = ( a1[ 0 ] == 't' ? true : false );
        }

        objs[ indexObject ].SetActive ( state );
    }

    private void TriggerSetVisible ( string a0, string a1 ) {
        int indexObject = ParseSomeInt ( a0 );

        if ( indexObject == -1 ) {
            Debug.Log ( "Can't parse " + a0 );
            return;
        }

        if ( objs.Length <= indexObject ) {
            Debug.Log ( "Object is out of range" );
            return;
        }

        if ( !objs[ indexObject ] ) {
            Debug.Log ( "Object is null" );
            return;
        }

        bool state;

        if ( a1[ 0 ] == 'n' ) {
            state = !objs[ indexObject ].activeSelf;
        } else {
            state = ( a1[ 0 ] == 't' ? true : false );
        }

        MeshRenderer mr = objs[ indexObject ].GetComponent<MeshRenderer> ();

        if ( mr ) {
            mr.enabled = state;
            return;
        }

        SkinnedMeshRenderer smr = objs[ indexObject ].GetComponent<SkinnedMeshRenderer> ();

        if ( smr ) {
            smr.enabled = state;
            return;
        }

        Light lgh = objs[ indexObject ].GetComponent<Light> ();

        if ( lgh ) {
            lgh.enabled = state;
            return;
        }
    }

    private void TriggerPlayAnimation ( string a0, string a1 ) {
        int indexObject = ParseSomeInt ( a0 );

        if ( indexObject == -1 ) {
            Debug.Log ( "Can't parse object index " + a0 );
            return;
        }

        if ( objs.Length <= indexObject ) {
            Debug.Log ( "Object is out of range" );
            return;
        }

        if ( !objs[ indexObject ] ) {
            Debug.Log ( "Object is null" );
            return;
        }

        string str;
        int indexString;

        if ( a1[ 0 ] == '\"' ) {
            str = a1.Substring ( 1, a1.Length - 2 );
        } else {
            indexString = ParseSomeInt ( a1 );

            if ( indexString == -1 ) {
                Debug.Log ( "Can't parse string index " + a1 );
                return;
            }

            if ( strings.Length <= indexString ) {
                Debug.Log ( "String is out of range" );
                return;
            }

            str = strings[ indexString ];
        }

        Animation anim = objs[ indexObject ].GetComponent<Animation> ();

        if ( !anim ) {
            Animator an = objs[ indexObject ].GetComponent<Animator> ();
            if ( !an ) {
                Debug.Log ( "No animation and animator component in GameObject" );
                return;
            }

            an.Play ( str );
            return;
        }

        if ( str == "" ) {
            anim.Play ();
        } else {
            anim.Play ( str );
        }
    }

    private void TriggerStopAnimation ( string a0, string a1 ) {
        int indexObject = ParseSomeInt ( a0 );

        if ( indexObject == -1 ) {
            Debug.Log ( "Can't parse object index " + a0 );
            return;
        }

        if ( objs.Length <= indexObject ) {
            Debug.Log ( "Object is out of range" );
            return;
        }

        if ( !objs[ indexObject ] ) {
            Debug.Log ( "Object is null" );
            return;
        }

        string str;
        int indexString;

        if ( a1[ 0 ] == '\"' ) {
            str = a1.Substring ( 1, a1.Length - 2 );
        } else {
            indexString = ParseSomeInt ( a1 );

            if ( indexString == -1 ) {
                Debug.Log ( "Can't parse string index " + a1 );
                return;
            }

            if ( strings.Length <= indexString ) {
                Debug.Log ( "String is out of range" );
                return;
            }

            str = strings[ indexString ];
        }

        Animation anim = objs[ indexObject ].GetComponent<Animation> ();

        if ( !anim ) {
            Animator an = objs[ indexObject ].GetComponent<Animator> ();
            if ( !an ) {
                Debug.Log ( "No animation and animator component in GameObject" );
                return;
            }

            an.StopPlayback ();
            return;
        }

        if ( str == "" ) {
            anim.Stop ();
        } else {
            anim.Stop ( str );
        }
    }

    private void TriggerSpeedAnimation ( string a0, string a1, string a2 ) {
        int indexObject = ParseSomeInt ( a0 );

        if ( indexObject == -1 ) {
            Debug.Log ( "Can't parse object index " + a0 );
            return;
        }

        if ( objs.Length <= indexObject ) {
            Debug.Log ( "Object is out of range" );
            return;
        }

        if ( !objs[ indexObject ] ) {
            Debug.Log ( "Object is null" );
            return;
        }

        string str;
        int indexString;

        if ( a1[ 0 ] == '\"' ) {
            str = a1.Substring ( 1, a1.Length - 2 );
        } else {
            indexString = ParseSomeInt ( a1 );

            if ( indexString == -1 ) {
                Debug.Log ( "Can't parse string index " + a1 );
                return;
            }

            if ( strings.Length <= indexString ) {
                Debug.Log ( "String is out of range" );
                return;
            }

            str = strings[ indexString ];
        }

        if ( str == "" ) {
            Debug.Log ( "Animation clip name is empty" );
            return;
        }

        int animSpeed = ParseSomeInt ( a2 );
        if ( animSpeed == -1 ) {
            Debug.Log ( "Can't parse animation speed " + a2 );
            return;
        }

        if ( animSpeed == 0 ) {
            Debug.Log ( "Animation speed have division by zero" );
            return;
        }

        float speed = animSpeed * 0.001f;

        Animation anim = objs[ indexObject ].GetComponent<Animation> ();

        if ( !anim ) {
            Debug.Log ( "No animation component in GameObject" );
            return;
        }

        AnimationClip clip = anim.GetClip ( str );

        if ( !clip ) {
            Debug.Log ( "Clip name " + str + " doesn't exist" );
            return;
        }

        anim[ str ].speed = speed;
    }

    private void TriggerDelay ( string a0 ) {
        int delay = ParseSomeInt ( a0 );

        if ( delay == -1 ) {
            Debug.Log ( "Can't parse delay " + a0 );
            NextLine ();
            return;
        }

        delayTime = Time.time + delay * 0.001f;

        isTrigDelay = true;
    }

    private void TriggerGoto ( string a0 ) {
        int index = ParseSomeInt ( a0 );
        if ( index == -1 ) {
            Debug.Log ( "Can't parse index " + a0 );
            currentLine++;
            NextLine ();
            return;
        }

        currentLine = ( uint ) index;
        NextLine ();
    }

    private void TriggerLockCursor ( string a0 ) {
        bool state;

        if ( a0[ 0 ] == 'n' ) {
            state = Cursor.lockState == CursorLockMode.None;
        } else {
            state = ( a0[ 0 ] == 't' ? true : false );
        }

        Cursor.lockState = ( state ? CursorLockMode.Locked : CursorLockMode.None );
    }

    private void TriggerShowCursor ( string a0 ) {
        bool state;

        if ( a0[ 0 ] == 'n' ) {
            state = !Cursor.visible;
        } else {
            state = ( a0[ 0 ] == 't' ? true : false );
        }

        Cursor.visible = state;
    }

    private void TriggerMoveSteps ( string a0, string a1, string a2 ) {
        int indexOrigin = ParseSomeInt ( a0 );
        int indexTarget = ParseSomeInt ( a1 );

        if ( indexOrigin == -1 || indexTarget == -1 ) {
            Debug.Log ( "Can't parse " + a0 + " or " + a1 );
            return;
        }

        if ( objs.Length <= Mathf.Max ( indexOrigin, indexTarget ) ) {
            Debug.Log ( "Index out of range" );
            return;
        }

        if ( !objs[ indexOrigin ] || !objs[ indexTarget ] ) {
            Debug.Log ( "objs[" + indexOrigin + "] or objs[" + indexTarget + "] doesn't exist" );
            return;
        }

        int steps = ParseSomeInt ( a2 );

        if ( steps == -1 ) {
            Debug.Log ( "Can't parse steps" );
            return;
        }

        if ( steps == 0 ) {
            Debug.Log ( "Steps have division by zero" );
            return;
        }

        MoveObjectBySteps mobs = objs[ indexOrigin ].GetComponent<MoveObjectBySteps> ();

        if ( mobs == null ) {
            Debug.Log ( "Script MoveObjectBySteps was null" );
            objs[ indexOrigin ].AddComponent<MoveObjectBySteps> ();
            mobs = objs[ indexOrigin ].GetComponent<MoveObjectBySteps> ();

            if ( mobs == null ) {
                Debug.Log ( "mobs почему-то null" );
            }
        }

        mobs.target = objs[ indexTarget ].transform;
        mobs.steps  = steps;

        mobs.MoveYourself ();
    }

    private void TriggerMoveTime ( string a0, string a1, string a2 ) {
        int indexOrigin = ParseSomeInt ( a0 );
        int indexTarget = ParseSomeInt ( a1 );

        if ( indexOrigin == -1 || indexTarget == -1 ) {
            Debug.Log ( "Can't parse " + a0 + " or " + a1 );
            return;
        }

        if ( objs.Length <= Mathf.Max ( indexOrigin, indexTarget ) ) {
            Debug.Log ( "Index out of range" );
            return;
        }

        if ( !objs[ indexOrigin ] || !objs[ indexTarget ] ) {
            Debug.Log ( "objs[" + indexOrigin + "] or objs[" + indexTarget + "] doesn't exist" );
            return;
        }

        int time = ParseSomeInt ( a2 );

        if ( time == -1 ) {
            Debug.Log ( "Can't parse steps" );
            return;
        }

        if ( time == 0 ) {
            Debug.Log ( "Steps have division by zero" );
            return;
        }

        MoveObjectByTime mobt = objs[ indexOrigin ].GetComponent<MoveObjectByTime> ();

        if ( mobt == null ) {
            Debug.Log ( "Script MoveObjectBySteps was null" );
            mobt = objs[ indexOrigin ].AddComponent<MoveObjectByTime> ();

            if ( mobt == null ) {
                Debug.Log ( "mobs почему-то null" );
            }
        }

        mobt.target = objs[ indexTarget ].transform;
        mobt.time = time;

        mobt.MoveYourself ();
    }

    public void Parse ( ) {
        if ( isTriggered || isStay ) {
            Debug.Log ( "Can't trigger because:" );
            if ( isTriggered ) Debug.Log ( "isTriggered = true" );
            if ( isStay ) Debug.Log ( "isStay = true" );
            return;
        }

        isTriggered = true;
        isStay = true;

        if ( code.Length == 0 ) {
            Debug.Log ( "Empty code" );
            isTriggered = false;
            return;
        }

        if ( !isParsed ) {
            isParsed = true;

            int codeCount = 0;
            for ( int i = 0; i < code.Length; i++ ) {
                codeCount += code[ i ].Split ( QUOTES, StringSplitOptions.RemoveEmptyEntries ).Length;
            }

            if ( codeCount != code.Length ) {
                string[] tmp = new string[ codeCount ];

                int k = 0;
                for ( int i = 0; i < code.Length; i++ ) {
                    string[] split = code[ i ].Split ( QUOTES, StringSplitOptions.RemoveEmptyEntries );

                    if ( split.Length == 0 ) {
                        continue;
                    }

                    for ( int j = 0; j < split.Length; j++ ) {
                        tmp[ k ] = split[ j ];
                        k++;
                    }
                }

                code = tmp;
            }

            for ( int i = 0; i < code.Length; i++ ) {
                string[] tmp = code[ i ].Split ( SPACE, StringSplitOptions.RemoveEmptyEntries );
                string some = tmp[ 0 ];
                for ( var j = 1; j < tmp.Length; j++ ) {
                    some += " " + tmp[ j ];
                }
                code[ i ] = some;
            }
        }

        currentLine = 0;

        NextLine ();
    }

    private void NextLine () {
        if ( code.Length <= currentLine ) {
            isTriggered = false;

            deltaTime = Time.time + delayBeforeReset * 0.001f;

            Debug.Log ( "Set timer to " + deltaTime );
            return;
        }

        if ( code[ currentLine ] == "" ) {
            Debug.Log ( "Empty line" );
            currentLine++;
            NextLine ();
            return;
        }

        string[] parser = code[ currentLine ].Split ( SPACE[ 0 ] );

        if ( parser.Length == 0 ) {
            Debug.Log ( "Blank parser" );
            currentLine++;
            NextLine ();
            return;
        }

        switch ( parser[ 0 ] ) {
            case "move":
                if ( parser.Length < 3 ) break;
                TriggerMove ( parser[ 1 ], parser[ 2 ] );
                break;

            case "lookat":
                if ( parser.Length < 3 ) break;
                TriggerLookAt ( parser[ 1 ], parser[ 2 ] );
                break;

            case "launch":
                if ( parser.Length < 3 ) break;
                TriggerLaunch ( parser[ 1 ], parser[ 2 ] );
                break;

            case "setactive":
                if ( parser.Length < 3 ) break;
                TriggerSetActive ( parser[ 1 ], parser[ 2 ] );
                break;

            case "setvisible":
                if ( parser.Length < 3 ) break;
                TriggerSetVisible ( parser[ 1 ], parser[ 2 ] );
                break;

            case "play":
                if ( parser.Length < 3 ) break;
                TriggerPlayAnimation ( parser[ 1 ], parser[ 2 ] );
                break;

            case "stop":
                if ( parser.Length < 3 ) break;
                TriggerStopAnimation ( parser[ 1 ], parser[ 2 ] );
                break;

            case "animationspeed":
                if ( parser.Length < 4 ) break;
                TriggerSpeedAnimation ( parser[ 1 ], parser[ 2 ], parser[ 3 ] );
                break;

            case "delay":
                if ( parser.Length < 2 ) break;
                TriggerDelay ( parser[ 1 ] );
                currentLine++;
                return;

            case "goto":
                if ( parser.Length < 2 ) break;
                TriggerGoto ( parser[ 1 ] );
                return;

            case "exit":
                Application.Quit ();
                break;

            case "showcursor":
                if ( parser.Length < 2 ) break;
                TriggerShowCursor ( parser[ 1 ] );
                break;

            case "lockcursor":
                if ( parser.Length < 2 ) break;
                TriggerLockCursor ( parser[ 1 ] );
                break;

            // User scripts
            case "movesteps":
                if ( parser.Length < 4 ) break;
                TriggerMoveSteps ( parser[ 1 ], parser[ 2 ], parser[ 3 ] );
                break;

            case "movetime":
                if ( parser.Length < 4 ) break;
                TriggerMoveTime ( parser[ 1 ], parser[ 2 ], parser[ 3 ] );
                break;
        }

        currentLine++;
        NextLine ();
    }

    private void Update () {
        if ( isTrigDelay && delayTime < Time.time ) {
            isTrigDelay = false;
            NextLine ();
            return;
        }

        if ( !isTriggered && isStay && deltaTime < Time.time && !once ) {
            isStay = false;
        }
    }
}
