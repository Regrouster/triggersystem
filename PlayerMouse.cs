﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMouse : MonoBehaviour {
    public Camera cam;
    public int buttonIndex = 0;
    public float maxDistance = 1.5f;
    public string[] tags;

    void Update () {
        if ( !Input.GetMouseButtonDown ( buttonIndex ) ) return;

        if ( !cam ) return;

        RaycastHit meow;
        Ray ray = cam.ScreenPointToRay ( Input.mousePosition );

        if ( !Physics.Raycast ( ray, out meow, maxDistance ) ) {
            return;
        }

        BTNTrigger btnt = meow.collider.gameObject.GetComponent<BTNTrigger> ();

        if ( btnt == null ) {
            return;
        }

        btnt.Use ();
    }
}
